fs = require 'fs'
path = require 'path'
{exec} = require 'child_process'
UglifyJS = require 'uglify-js'
colors = require 'colors'
coffeelint = require 'coffeelint'

paths = {}
for dir in ['dist', 'src']
  paths["#{dir}Dir"] = dir

for dir in [paths.distDir]
  fs.mkdirSync dir, '0755' if not fs.existsSync dir

packageInfo = JSON.parse fs.readFileSync path.join __dirname, 'package.json'

coffeeLintConfig =
  no_tabs:
    level: 'error'
  no_trailing_whitespace:
    level: 'error'
  max_line_length:
    value: 80
    level: 'error'
    limitComments: false
  camel_case_classes:
    level: 'error'
  indentation:
    value: 2
    level: 'error'
  no_implicit_braces:
    level: 'ignore'
  no_trailing_semicolons:
    level: 'error'
  no_plusplus:
    level: 'ignore'
  no_throwing_strings:
    level: 'error'
  no_backticks:
    level: 'warn'
  line_endings:
    value: 'any'

task 'build', 'Compiles JavaScript file for production use', ->
  console.log "Compiling CoffeeScript".yellow
  exec " coffee -c -b --no-header -m -o #{paths.distDir} #{paths.srcDir}/noquery.coffee", (e, o, se) ->
    if e
      console.error "Error encountered while compiling CoffeeScript".red
      console.error se
      process.exit 1

    console.log "CoffeeScript Compiled"
  uglifyOptions =
  warnings: true
  mangle: true
  toplevel: true
  output:
    comments: "some"
  compress:
    typeofs: false
  sourceMap:
    content: fs.readFileSync("#{paths.distDir}/noquery.js.map", 'utf8')
    url: 'noquery.min.js.map'

  console.log "Compressing with UglifyJS".yellow
  outputPath = "#{packageInfo.name} #{packageInfo.version}"
  result = UglifyJS.minify({ 'noquery.js': fs.readFileSync("#{paths.distDir}/noquery.js", 'utf8') }, uglifyOptions)
  fs.writeFileSync "#{paths.distDir}/noquery.min.js", result.code, 'utf8'
  fs.writeFileSync "#{paths.distDir}/noquery.min.js.map", result.map, 'utf8'
  console.log "JavaScript minified: " + outputPath.green
  invoke 'size'

task 'lint', 'Check CoffeeScript for lint', ->
  console.log "Checking *.coffee for lint".yellow
  pass = "✔".green
  warn = "⚠".yellow
  fail = "✖".red
  getSourceFilePaths().forEach (filepath) ->
    fs.readFile filepath, (err, data) ->
      shortPath = filepath.substr paths.srcDir.length + 1
      result = coffeelint.lint data.toString(), coffeeLintConfig
      if result.length
        hasError = result.some (res) -> res.level is 'error'
        level = if hasError then fail else warn
        console.error "#{level}  #{shortPath}".red
        for res in result
          level = if res.level is 'error' then fail else warn
          console.error "   #{level}  Line #{res.lineNumber}: #{res.message}"
      else
        console.log "#{pass}  #{shortPath}".green

# Helper for finding all source files
getSourceFilePaths = (dirPath = paths.srcDir) ->
  files = []
  for file in fs.readdirSync dirPath
    filepath = path.join dirPath, file
    stats = fs.lstatSync filepath
    if stats.isDirectory()
      files = files.concat getSourceFilePaths filepath
    else if /\.coffee$/.test file
      files.push filepath
  files

task 'clean', 'Remove temporary and generated files', ->
  for file in fs.readdirSync paths.distDir
    filepath = path.join paths.distDir, file
    if /\.js$/.test or /\.map$/.test filepath
      fs.unlinkSync filepath
      console.log "Removed #{filepath}".magenta

task 'size', 'Report file size', ->
  console.log "Checking output sizes".yellow
  return if not fs.existsSync paths.distDir
  for file in fs.readdirSync paths.distDir
    # Skip non-JS files
    if /\.js$/.test file
      stats = fs.statSync path.join paths.distDir, file
      console.log "#{file}: #{stats.size} bytes"
