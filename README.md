# noQuery

Some JavaScript for when you want some of jQuery but not all.

This library is a work in progress.

## Installation

You can either download the built files directly or you can add this library to your project using `npm` or `yarn`:

    $ yarn add -D noquery

or

    $ npm install --save-dev noquery

## Usage

Include either the minified or the plain JS in your `<head>`:

```html
<script src="noquery.js"></script>
<!-- or: -->
<script src="noquery.min.js"></script>
```

Or, if you are using something like Jekyll then you can copy the source file
`noquery.coffee` into your projekt and have Jekyll compile it for you.

## Functions

Now you can use the functions in your JS. Noquery exports the following functions:

* `addClass(element, className)`
* `removeClass(element, className)`
* `toggleClass(element, className)`
* `matches(element, selector)`

#### addClass

Adds a class to an element. The element is any valid element from the DOM.

##### Example
```javascript
var divElement = document.getElementById('some-div');
addClass(divElement, "new-class");
```

#### removeClass

Removes a class from an element. The element is any valid element from the DOM.

##### Example
```javascript
var divElement = document.getElementById('some-div');
removeClass(divElement, "new-class");
```

#### toggleClass

Toggles the presence of a class on an element. The element is any valid element from the DOM.

##### Example
```javascript
var divElement = document.getElementById('some-div');
toggleClass(divElement, "hidden");
```

#### matches

Checks if an element matches a selector, meant to replace the jQuery `.is()` function.

##### Example
```javascript
var checkBox = document.getElementById('some-checkbox');
if (matches(checkBox, ":checked")) {
  // Do something.
} else {
  // Do something else.
}
```

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/robotika-ax/noquery. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The library is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
