###!
# noquery (c) Jan Lindblom 2020
#
# @version: 0.1.0
# @license: MIT
###

#! Function creator
createFun = (name, definition) ->
  if typeof module != 'undefined'
    module.exports = definition()
  else if typeof define == 'function' and typeof define.amd == 'object'
    define definition
  else
    @[name] = definition()
  return

#! Create addClass function
createFun 'addClass', ->
  (el, className) ->
    if el.classList
      el.classList.add className
    else
      current = el.className
      found = false
      all = current.split(' ')
      i = 0
      while i < all.length and !found
        found = all[i] == className
        i++
      if !found
        if current == ''
          el.className = className
        else
          el.className += ' ' + className
    return

#! Create removeClass function
createFun 'removeClass', ->
  (el, className) ->
    if el.classList
      el.classList.remove className
    else
      pattern = '(^|\\b)' + className.split(' ').join('|') + '(\\b|$)'
      el.className = el.className.replace(new RegExp(pattern, 'gi'), ' ')
    return

#! Create toggleClass function
createFun 'toggleClass', ->
  (el, className) ->
    if el.classList
      el.classList.toggle className
    else
      classes = el.className.split(' ')
      existingIndex = classes.indexOf(className)
      if existingIndex >= 0
        classes.splice existingIndex, 1
      else
        classes.push className
      el.className = classes.join(' ')
    return

###! Create matches function
# This can be used instead of `is` so that, for example this:
#   $( this ).is( ":first-child" )
# becomes:
#   matches(this, ":first-child")
###
createFun 'matches', ->
  (el, selector) ->
    # coffeelint: disable=max_line_length
    (el.matches or el.matchesSelector or el.msMatchesSelector or el.mozMatchesSelector or el.webkitMatchesSelector or el.oMatchesSelector).call el, selector
    # coffeelint: enable=max_line_length
