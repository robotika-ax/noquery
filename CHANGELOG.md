# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][Keep a Changelog] and this project adheres to [Semantic Versioning][Semantic Versioning].


## [Unreleased]

More functions to be added:

* onEvent

---

## [0.1.0] - 2020-07-06

### Added

Functions:

* addClass
* removeClass
* toggleClass
* matches

---

[Keep a Changelog]: https://keepachangelog.com/
[Semantic Versioning]: https://semver.org/

[Unreleased]: https://bitbucket.org/robotika-ax/noquery/branches/compare/v0.1.0..HEAD
[0.1.0]: https://github.com/robotika-ax/noquery/compare/v0..v0.1.0
