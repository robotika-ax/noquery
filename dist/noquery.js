/*!
 * noquery (c) Jan Lindblom 2020
 *
 * @version: 0.1.0
 * @license: MIT
 */
var createFun;

//! Function creator
createFun = function(name, definition) {
  if (typeof module !== 'undefined') {
    module.exports = definition();
  } else if (typeof define === 'function' && typeof define.amd === 'object') {
    define(definition);
  } else {
    this[name] = definition();
  }
};

//! Create addClass function
createFun('addClass', function() {
  return function(el, className) {
    var all, current, found, i;
    if (el.classList) {
      el.classList.add(className);
    } else {
      current = el.className;
      found = false;
      all = current.split(' ');
      i = 0;
      while (i < all.length && !found) {
        found = all[i] === className;
        i++;
      }
      if (!found) {
        if (current === '') {
          el.className = className;
        } else {
          el.className += ' ' + className;
        }
      }
    }
  };
});

//! Create removeClass function
createFun('removeClass', function() {
  return function(el, className) {
    var pattern;
    if (el.classList) {
      el.classList.remove(className);
    } else {
      pattern = '(^|\\b)' + className.split(' ').join('|') + '(\\b|$)';
      el.className = el.className.replace(new RegExp(pattern, 'gi'), ' ');
    }
  };
});

//! Create toggleClass function
createFun('toggleClass', function() {
  return function(el, className) {
    var classes, existingIndex;
    if (el.classList) {
      el.classList.toggle(className);
    } else {
      classes = el.className.split(' ');
      existingIndex = classes.indexOf(className);
      if (existingIndex >= 0) {
        classes.splice(existingIndex, 1);
      } else {
        classes.push(className);
      }
      el.className = classes.join(' ');
    }
  };
});

/*! Create matches function
 * This can be used instead of `is` so that, for example this:
 *   $( this ).is( ":first-child" )
 * becomes:
 *   matches(this, ":first-child")
 */
createFun('matches', function() {
  return function(el, selector) {
    // coffeelint: disable=max_line_length
    return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
  };
});

// coffeelint: enable=max_line_length

//# sourceMappingURL=noquery.js.map
